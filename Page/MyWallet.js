import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView, FlatList } from 'react-native';
import bg from '../image/357.jpg'
import bg2 from '../image/3.jpg'
import red from '../image/og.jpg'
import blue from '../image/2.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, Card, WingBlank } from '@ant-design/react-native';

import { connect } from 'react-redux'

class MyWallet extends React.Component {

    state = {
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    UNSAFE_componentWillMount = () => {
        console.log('wallet props', this.props.selectwallet);
        let selected = this.props.wallets.find(ele => { return ele._id === this.props.selectwallet._id })
        this.setState({ wallet: selected })
    }

    getIncomePerDay = (list) => {
        let income = 0;
        list.forEach(ele => {
            if (ele.type === 'Revenue') income += ele.money
        })
        return income
    }

    getExpensePerDay = (list) => {
        let expense = 0;
        list.forEach(ele => {
            if (ele.type === 'Expense') expense += ele.money
        })
        return expense
    }

    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'Today'
        return dateToString(date)
    }


    goToMain = () => {
        this.props.history.push('./main')
    }

    goToWallet = () => {
        this.props.history.push('./wallet')
    }
    goToRevenue = () => {
        this.props.history.push('./revenue')
    }
    goToExpense = () => {
        this.props.history.push('./expense')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <ImageBackground style={styles.iconhead1} source={bg}>
                        <TouchableOpacity onPress={this.goToMain} >
                            <Icon size='lg' color='white' name={'arrow-left'} />
                        </TouchableOpacity>
                    </ImageBackground>

                    <ImageBackground style={styles.iconhead2} source={bg}>
                        <Text style={styles.text}>{this.state.wallet.name}</Text>
                    </ImageBackground>

                </View>
                <View style={styles.body}>
                    <ImageBackground source={bg2} style={{ width: '100%', height: '100%' }}>

                        <View style={styles.amountMoney}>
                            <View style={styles.image}>
                                <View style={styles.inputtex}>
                                    <Text style={styles.text1}>{parseFloat(this.state.wallet.balance).toFixed(2)}</Text>
                                </View>
                                <Image source={logo} style={styles.image} />
                            </View>

                        </View>

                        <Text></Text>

                        <ScrollView style={styles.listWallet}>
                            <WingBlank size="lg">
                                <FlatList
                                    data={this.state.wallet.transactions}
                                    extraData={this.state} s
                                    renderItem={({ item }) => (
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <View style={[{ width: '92%', height: 30, backgroundColor: 'white', borderRadius: 15 }, styles.centering]}>
                                                <Text style={styles.texttext}>{this.formatDate(item.date)}</Text>
                                            </View>
                                            <View style={{ width: '100%' }}>
                                                <View style={{ width: '100%', height: 36, flexDirection: 'row', justifyContent: 'space-evenly', alignItems: 'center' }}>
                                                    <Text style={{ fontWeight: 'bold', color: '#33FF33' }}>Revenue: {parseFloat(this.getIncomePerDay(item.list)).toFixed(2)}</Text>
                                                    <Text style={{ fontWeight: 'bold', color: '#FF3333' }}>Expense: {parseFloat(this.getExpensePerDay(item.list)).toFixed(2)}</Text>
                                                    <Text style={{ fontWeight: 'bold', color: 'white' }}>Summary: {parseFloat(this.getIncomePerDay(item.list) - this.getExpensePerDay(item.list)).toFixed(2)}</Text>
                                                </View>
                                                <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.15)' }} />
                                            </View>
                                            <View style={{ width: '84%' }}>
                                                {item.list.map((ele) => {
                                                    return (
                                                        <View style={{ width: '100%' }}>
                                                            <View style={{ width: '100%', height: 36, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                                                <Text style={{ color: 'white' }}>{ele.type}</Text><Text style={{ color: 'white' }}>{ele.catagory}</Text><Text style={{ color: 'white' }}>{parseFloat(ele.money).toFixed(2)}</Text>
                                                            </View>
                                                            <View style={{ width: '100%', height: 1, backgroundColor: 'rgba(0,0,0,0.15)' }} />
                                                        </View>
                                                    )
                                                })}
                                            </View>
                                        </View>
                                    )}
                                />
                            </WingBlank>
                        </ScrollView>
                        <Text></Text>
                        <Text></Text>
                        <Text></Text>
                    </ImageBackground>
                </View>

                <View style={styles.footer}>
                    <ImageBackground style={styles.iconbar} source={blue}>
                        <TouchableOpacity onPress={this.goToRevenue}>
                            <Icon size='lg' color='white' name={'plus'} style={{ left: 20 }} />
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>REVENUNE</Text>
                        </TouchableOpacity>
                    </ImageBackground>
                    <ImageBackground style={styles.iconbar} source={red}>
                        <TouchableOpacity onPress={this.goToExpense}>
                            <Icon size='lg' color='white' name={'plus'} style={{ left: 10 }} />
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>EXPENSE</Text>
                        </TouchableOpacity>
                    </ImageBackground>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        borderWidth: 5,
        borderColor: 'white',
        flexDirection: 'row'
    },
    iconhead1: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    iconhead2: {
        flex: 5,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    body: {
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        borderWidth: 3,
        borderColor: 'white',
        flexDirection: 'row'
    },
    text: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center'
    },
    text1: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center'
    },
    amountMoney: {

    },
    listWallet: {
        flex: 1,
        top: 30
    },
    iconbar: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    image: {
        width: 120,
        height: 120,
        borderRadius: 100,
        top: -55,
        left: 5
    },
    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 260,
        height: 90,
        margin: 14,
        top: 65,
        left: 60,
        borderRadius: 100,
    },
    centering: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    texttext: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
    },
})

const mapStateToProps = (state) => {
    return {
        selectwallet: state.selectwallet,
        wallets: state.wallets
    }
}

export default connect(mapStateToProps)(MyWallet)

