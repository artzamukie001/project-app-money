import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/22.png'
import { Button, Icon } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios'

class profile extends React.Component {

    state = {
        imagePath: '',
        isLoading: false
    }


    componentDidMount() {
        this.getImage()
    }

    goToLoginPage = () => {
        return this.props.history.push('/LoginPage')
    }

    getImage = () => {
        const { user } = this.props
        this.setState({ isLoading: true })
        axios.get('https://zenon.onthewifi.com/moneyGo/users', {
            headers: {
                Authorization: `Bearer ${this.props.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => {
                this.setState({ isLoading: false })
                console.log(err)
            })
            .finally(() => { console.log('Finally') })
    }


    goToLogin = () => {
        this.props.history.push('./Login')
    }

    goToMain = () => {
        this.props.history.push('./main')
    }

    goToWallet = () => {
        this.props.history.push('./wallet')
    }

    goToEdit = () => {
        this.props.history.push('./EditProfile')
    }

    render() {
        return (
            <ImageBackground source={bg} style={styles.container}>
                
                    <Text style={styles.text}>My Profile</Text>
                    
                        <ImageBackground style={styles.image} source={logo} >
                            <Image source={{ uri: this.state.imagePath }} style={styles.image} />
                        </ImageBackground>
                    


                    <View style={styles.inputtex}>
                        <Text style={styles.text1}>
                            Email : {this.props.user.email}
                        </Text>
                        <Icon style={styles.icon} name={"mail"} />
                    </View>

                    {/* <View style={styles.inputtex}>
                        <Text style={styles.text1}>
                            Chang Paswoord
                    </Text>
                        <Icon style={styles.icon} name={"mail"} />
                    </View> */}

                    <View style={styles.inputtex}>
                        <Text style={styles.text1}>
                            First Name : {this.props.user.firstName}
                        </Text>
                        <Icon style={styles.icon} name={"user"} />
                    </View>

                    <View style={styles.inputtex}>
                        <Text style={styles.text1}>
                            Last Name : {this.props.user.lastName}
                        </Text>
                        <Icon style={styles.icon} name={"user"} />
                    </View>

                    <View style={styles.row}>
                        <TouchableOpacity>
                            <Button type="primary"
                                onPress={this.goToEdit}>EDIT</Button>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <Button type="warning" style={{ marginLeft: 20 }}
                                onPress={this.goToLogin}>LOGOUT</Button>
                        </TouchableOpacity>
                    </View>
             
                <Text></Text>
               

                <View style={styles.footer}>
                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToMain}>
                            <Icon size='lg' color='white' name={'home'} />
                        </TouchableOpacity>
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToWallet} >
                            <Icon size='lg' color='white' name={'wallet'} />
                        </TouchableOpacity>
                    </ImageBackground>


                    <ImageBackground style={styles.iconbar} source={bg}>
                        <Icon size='lg' color='white' name={'bell'} />
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToProfile} >
                            <Icon size='lg' color='white' name={'user'} />
                        </TouchableOpacity>
                    </ImageBackground>
                </View>

            </ImageBackground>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
        padding: 20
    },
    image: {
        width: 150,
        height: 150,
    },
    image2: {
        width: 150,
        height: 150,
        borderRadius: 300,
    },
    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 55,
        margin: 14,
        borderRadius: 100,
    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37
    },
    listWallet: {
        flex: 1,
        top: 30,
    },
    footer: {
        borderWidth: 3,
        borderColor: 'white',
        flexDirection: 'row'
    },
    iconbar: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    listWallet: {
        flex: 1,
        alignItems: 'center',
        justifyContent:'center'
    },
})

export default connect(mapStateToProps)(profile)
