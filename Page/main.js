import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView, FlatList } from 'react-native';
import bg from '../image/357.jpg'
import bg2 from '../image/3.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, Card, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push, replace } from 'connected-react-router'



class main extends React.Component {

    state = {
        totalBalance: '',
        wallets: []
    }

    UNSAFE_componentWillMount() {
        this.setState({ wallets: this.props.wallets })
    }

    componentDidMount = () => {
        this.getTotalBalance()
    }


    getTotalBalance = () => {
        let total = 0
        this.props.wallets.forEach(ele => {
            total += ele.balance
        })
        this.setState({ totalBalance: total })
        return total
    }


    goToWallet = () => {
        this.props.history.push('/wallet')
    }

    goToAddWallet = () => {
        this.props.history.push('/AddWallet')
    }

    goToProfile = () => {
        this.props.history.push('/profile')
    }

    goToMyWallet = (wallet) => {
        this.props.goToMyWallet(wallet)
        this.props.history.push('/MyWallet')
    }

    goToMain = () => {
        this.props.history.push('./main')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <ImageBackground style={{ width: null, height: null }} source={bg}>
                        <Text style={styles.text}>Money Go</Text>
                    </ImageBackground>
                </View>
                <View style={styles.body}>
                    <ImageBackground source={bg2} style={{ width: '100%', height: '100%' }}>
                        <View style={styles.amountMoney}>
                            <View style={styles.image}>
                                <View style={styles.inputtex}>
                                    <Text style={styles.text1}>{parseFloat(this.state.totalBalance).toFixed(2)}</Text>
                                </View>
                                <Image source={logo} style={styles.image} />
                            </View>

                        </View>

                        <ScrollView style={styles.listWallet}>
                            <WingBlank size="lg">
                                <Card>
                                    <TouchableOpacity onPress={this.goToAddWallet}>
                                        <Card.Body style={{ color: 'gray' }}>
                                            <Icon style={styles.icon} name={"plus"} size={45} />
                                            <View style={{ height: 42 }}>

                                                <Text style={styles.textAddWallet}
                                                >Add Wallet</Text>

                                            </View>
                                        </Card.Body>
                                    </TouchableOpacity>

                                </Card>

                                <FlatList
                                    data={this.state.wallets}
                                    style={{ width: '100%' }}
                                    keyExtractor={(item) => item._id}
                                    renderItem={({ item }) => (
                                        <TouchableOpacity onPress={() => this.goToMyWallet(item)}
                                            style={styles.wallet}
                                        >
                                            <Text style={{ fontSize: 24 }}>{item.name}</Text>
                                            <Text style={{ fontSize: 24 }}>{Number.parseFloat(item.balance).toFixed(2)}</Text>
                                        </TouchableOpacity>
                                    )}
                                />
                            </WingBlank>
                        </ScrollView>

                    </ImageBackground>
                </View>

                <View style={styles.footer}>
                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity>
                            <Icon size='lg' color='white' name={'home'} />
                        </TouchableOpacity>
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToWallet} >
                            <Icon size='lg' color='white' name={'wallet'} />
                        </TouchableOpacity>
                    </ImageBackground>


                    <ImageBackground style={styles.iconbar} source={bg}>
                        <Icon size='lg' color='white' name={'bell'} />
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToProfile} >
                            <Icon size='lg' color='white' name={'user'} />
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        borderWidth: 5,
        borderColor: 'white'
    },
    body: {
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        borderWidth: 3,
        borderColor: 'white',
        flexDirection: 'row'
    },
    text: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center'

    },
    text1: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center'
    },
    amountMoney: {

    },
    listWallet: {
        flex: 1,
        top: 30
    },
    iconbar: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    image: {
        width: 120,
        height: 120,
        borderRadius: 100,
        top: -50,
        left: 5
    },
    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 260,
        height: 90,
        margin: 14,
        top: 70,
        left: 60,
        borderRadius: 100,
    },
    inputtex1: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 300,
        height: 65,
        margin: 14,
        top: 70,
        left: 10,
        borderRadius: 100,
    },
    wallet: {
        height: 80,
        backgroundColor: '#f4f4f4',
        paddingHorizontal: 24,
        margin: 8,
        borderRadius: 6,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    icon: {
        position: 'absolute',
        top: 12,
        left: 37,
    },
    textAddWallet: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center',
        top: -15
    },
})

const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        goToMyWallet: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        },
        push: (path) => {
            dispatch(replace(path))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(main)

