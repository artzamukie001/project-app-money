import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'

class AddWallet extends React.Component {

    state = {
        walletName: '',
        initialMoney: ''
    }

    addNewWallet = () => {
        let newId = 0
        if (this.props.wallets.length > 0)
            newId = parseInt(this.props.wallets[this.props.wallets.length - 1]._id, 10) + 1
        const wallet = {
            _id: newId.toString(),
            name: this.state.walletName === '' ? 'My Wallet' : this.state.walletName,
            balance: this.state.initialMoney === '' ? 0.00 : parseFloat(this.state.initialMoney),
            transactions: []
        }
        this.props.addWallet(wallet)
        Alert.alert('Add New Wallet', 'Successful')
        return this.props.history.push('/main')
    }


    goToWallet = () => {
        this.props.history.push('/wallet')
    }

    goToMain = () => {
        this.props.history.push('/main')
    }
    

    render() {
        return (
            <ImageBackground source={bg} style={styles.container}>
                <Text style={styles.text}>Add New Wallet</Text>
                <View style={styles.image}>
                    <Image source={logo} style={styles.image} />
                </View>

                <Text></Text>

                {/* <Icon size={30} name={"wallet"} /> */}
                <InputItem style={styles.text1}
                    value={this.state.walletName}
                    onChange={value => {this.setState({ walletName: value })}}
                    placeholder='Name Wallet'
                    style={styles.inputtex}
                />

                <Text></Text>

                <InputItem style={styles.text1}
                    value={this.state.initialMoney}
                    onChange={value => { this.setState({ initialMoney: value }) }}
                    type='number'
                    placeholder='Initial Money'
                    style={styles.inputtex}
                />

                <Text></Text>

                <View style={styles.row}>
                    <TouchableOpacity>
                        <Button type="primary"
                            onPress={this.addNewWallet}>SAVE</Button>
                    </TouchableOpacity>
                    <Text></Text>
                    <Button type="warning"  style={{ marginLeft: 20 }}
                        onPress={this.goToMain}>CANCEL</Button>
                </View>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20,
    },
    image: {
        backgroundColor: '#99FFCC',
        width: 150,
        height: 150,
        borderRadius: 100,
    },
    inputtex: {
        borderWidth: 1,
        borderColor: '#d7dae0',
        borderRadius: 100,
        paddingHorizontal: 14,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50

    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37,
    }
})

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addWallet: (wallet) => {
            dispatch({
                type: 'ADD_WALLET',
                payload: wallet
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddWallet)


