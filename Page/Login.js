import React, { Component } from 'react'
import { connect } from 'react-redux';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, InputItem } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import axios from 'axios'

class Login extends React.Component {
    state = {
        email: '',
        password: ''
    }

    login = async () => {
        try {
            const res = await axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.email,
                password: this.state.password
            })
            const user = res.data.user
            await this.props.userLogin(user)
            await this.props.history.push('/main')
        } catch (error) {
            console.log(JSON.stringify(err))
            const err = error.response.data.errors ? error.response.data.errors : error
            console.log('login response', error.response.data.errors);
            let errorMessage = ''
            if (err.email) errorMessage += 'Email or Password: ' + err.email + '\n'
            if (err.password) errorMessage += 'Email or Password: ' + err.password + '\n'
            Alert.alert('Incorrect Login', errorMessage)
        }
    }

    signup = () => {
        this.props.history.push('/Register')
    }
    
    goToRegister = () => {
        this.props.history.push('./Register')
    }

    goToMain = () => {
        this.props.history.push('./main')
    }

    render() {
        return (
            <ImageBackground source={bg} style={styles.container}>
                <Text style={styles.text}>MoneyGo</Text>
                <View style={styles.image}>
                    <Image source={logo} style={styles.image} />
                </View>

                <Text></Text>

                <InputItem
                    value={this.state.email}
                    onChange={value => this.setState({ email: value })}
                    placeholder="Email"
                    style={styles.inputtex}
                />
                <Text></Text>

                <InputItem
                    type='password'
                    value={this.state.password}
                    onChange={value => { this.setState({ password: value }) }}
                    placeholder="Password"
                    style={styles.inputtex}
                />

                <Text></Text>

                <View style={styles.row}>
                    <Button type="primary"
                        onPress={this.login}>Login</Button>
                    <Text></Text>
                    <TouchableOpacity>
                        <Button type="warning" style={{ marginLeft: 20 }}
                            onPress={this.goToRegister}>Register</Button>
                    </TouchableOpacity>
                </View>
                <Text></Text>
                <Button type="primary">Login With Facebook</Button>
            </ImageBackground>

        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'USER_LOGIN',
                user: user
            })
        }
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20
    },
    image: {
        backgroundColor: '#99FFCC',
        width: 150,
        height: 150,
        borderRadius: 100,
    },
    inputtex: {
        borderWidth: 1,
        borderColor: '#d7dae0',
        borderRadius: 100,
        paddingHorizontal: 14,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',

    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37
    }
})

export default connect(null, mapDispatchToProps)(Login)
