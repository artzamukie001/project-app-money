import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/11111.png'
import { Button, Icon, Card, WingBlank, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import ModalDropdown from 'react-native-modal-dropdown';



class revenue extends React.Component {

    state = {
        catagory: '',
        description: '',
        money: '',
        image: ''
    }

    // types = [{ value: 'Salary', image: require('../../assets/types/salary.png') },
    // { value: 'Extra Income', image: require('../../assets/types/extraincome.png') }]

    onChangeTypeSelect = (index, value) => {
        this.setState({ catagory: value, image: this.types[index].image })
    }

    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'Today'
        return dateToString(date)
    }

    addNewTransaction = () => {
        let updateWallet = {}
        const todayDate = new Date()
        const newTransaction = {
            type: 'Revenue',
            description: this.state.description,
            catagory: this.state.catagory,
            money: parseInt(this.state.money, 10)
        }
        if (this.props.selectwallet.transactions[0] && this.props.selectwallet.transactions[0].date) {
            if (this.formatDate(this.props.selectwallet.transactions[0].date) === this.formatDate(todayDate)) {
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: this.props.selectwallet.transactions
                }
                updateWallet.transactions[0].list.push(newTransaction)
            } else {
                const newTransactions = [{
                    date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                    list: [newTransaction]
                }, ...this.props.selectwallet.transactions]
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance + newTransaction.money,
                    transactions: newTransactions
                }
            }
        } else {
            const newTransactions = [{
                date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                list: [newTransaction]
            }, ...this.props.selectwallet.transactions]
            updateWallet = {
                ...this.props.selectwallet,
                balance: this.props.selectwallet.balance + newTransaction.money,
                transactions: newTransactions
            }
        }
        this.props.updateWallet(updateWallet)
        this.props.clickWallet(updateWallet)
        this.props.history.replace('/MyWallet')
    }

    goToMyWallet = () => {
        this.props.history.push('/MyWallet')
    }

    render() {
        return (
            <ImageBackground source={bg} style={styles.container}>
                <Text style={styles.text}>Revenue</Text>
                <View style={styles.image}>
                    <Image source={logo} style={styles.image} />
                </View>

                <Text></Text>

                {/* <Icon size={30} name={"wallet"} /> */}
                <Text style={{ fontSize: 20, color: 'white', left: 5, fontWeight: 'bold' }}>Money</Text>
                <Text></Text>
                <InputItem
                    type='number'
                    placeholder='Enter Money...'
                    value={this.state.money}
                    onChange={value => { this.setState({ money: value }) }}
                    style={styles.inputtex}
                />


                <Text></Text>
                <Text style={{ fontSize: 20, color: 'white', left: 5, fontWeight: 'bold' }}>Description</Text>
                <Text></Text>
                <InputItem
                    style={styles.inputtex}
                    value={this.state.catagory}
                    onChange={(value) => { this.setState({ catagory: value }) }}
                    placeholder='Enter Description...' />


                <Text></Text>

                <View style={styles.row}>
                    <TouchableOpacity >
                        <Button type="primary" onPress={this.addNewTransaction}>SAVE</Button>
                    </TouchableOpacity>

                    <Text></Text>
                    <TouchableOpacity>
                        <Button type="warning" style={{ marginLeft: 20 }} onPress={this.goToMyWallet}>CANCEL</Button>
                    </TouchableOpacity>
                </View>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        fontSize: 20,
        fontWeight: 'bold',
        padding: 20,
    },
    image: {
        backgroundColor: 'white',
        width: 150,
        height: 150,
        borderRadius: 100,
    },
    inputtex: {
        borderWidth: 1,
        borderColor: '#d7dae0',
        borderRadius: 100,
        paddingHorizontal: 14,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        height: 50

    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37,
    }
})

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets,
        selectwallet: state.selectwallet
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateWallet: (newWallet) => {
            dispatch({
                type: 'UPDATE_WALLET',
                payload: newWallet
            })
        },
        clickWallet: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(revenue)
