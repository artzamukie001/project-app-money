import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, InputItem, WhiteSpace } from '@ant-design/react-native';
import axios from 'axios'

class Register extends React.Component {

    state = {
        email: '',
        password: '',
        confirmPassword: '',
        firstName: '',
        lastName: ''
    }

    SignUp = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '' || this.state.firstName === '' || this.state.lastName === '') {
            Alert.alert('Please do not leave input fields blanked')
        } else {
            if (this.state.password !== this.state.confirmPassword) {
                await Alert.alert('Incorrect Input', 'Confirmed password is not matched.')
            } else {
                try {
                    await axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstName,
                        lastName: this.state.lastName
                    })
                    await Alert.alert('Register successful')
                    await this.props.history.replace('/Login')
                } catch (error) {
                    console.log('signup res', error.response.data.errors);
                }
            }
        }
    }

    goToLogin = () => {
        this.props.history.push('./Login')
    }

    render() {
        return (
            <ImageBackground source={bg} style={styles.container}>
                <ScrollView>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'space-evenly' }}>

                            <Text style={styles.text}>Register</Text>
                            <View style={styles.image}>
                                <Image source={logo} style={styles.image} />
                            </View>
                        </View>
                    </View>


                    <View style={{ width: '100%',top:20 }}>
                        <Text style={styles.text1}>Email:</Text>
                        <InputItem
                            clear
                            value={this.state.email}
                            onChange={value => this.setState({ email: value })}
                            style={styles.inputtex}
                        />
                        <Text style={styles.text1}>Password:</Text>
                        <InputItem
                            clear
                            type='password'
                            value={this.state.password}
                            onChange={value => { this.setState({ password: value }) }}
                            style={styles.inputtex}
                        />
                        <Text style={styles.text1}>Confirm Password:</Text>
                        <InputItem
                            clear
                            type='password'
                            value={this.state.confirmPassword}
                            onChange={value => { this.setState({ confirmPassword: value }) }}
                            style={styles.inputtex}
                        />
                        <Text style={styles.text1}>First Name:</Text>
                        <InputItem
                            clear
                            value={this.state.firstName}
                            onChange={value => { this.setState({ firstName: value }) }}
                            style={styles.inputtex}
                        />
                        <Text style={styles.text1}>Last Name</Text>
                        <InputItem
                            clear
                            value={this.state.lastName}
                            onChange={value => { this.setState({ lastName: value }) }}
                            style={styles.inputtex}
                        />
                    </View>

                    <Text></Text>
                    <Text></Text>

                    <View style={{ alignItems: 'center' }}>
                        <Button type='primary'
                            onPress={this.SignUp}
                        >
                            Submit
                            </Button>
                    </View>

                    <Text></Text>
                    <View style={{ alignItems: 'center' }}>
                        <Button type="warning"
                            onPress={this.goToLogin}>CANCEL
                        </Button>
                    </View>

                    <Text></Text>
                </ScrollView>
            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
    },

    text: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20,
        left: 10
    },
    text1: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        marginLeft: 22

    },
    image: {

        width: 150,
        height: 150,
        borderRadius: 100,
        left: 5
    },
    inputtex: {
        borderWidth: 1,
        borderColor: '#d7dae0',
        borderRadius: 6,
        paddingHorizontal: 14,
        backgroundColor: 'white',
        alignItems:'center',
        justifyContent:'center'

    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37
    },
    listWallet: {
        flex: 1,
        top: 30,
    },

})

export default Register
