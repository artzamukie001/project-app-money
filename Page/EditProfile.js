import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground } from 'react-native';
import bg from '../image/357.jpg'
import logo from '../image/22.png'
import { Button, Icon, InputItem } from '@ant-design/react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'

class EditProfile extends React.Component {

    state = {
        firstName: this.props.user.firstName,
        lastName: this.props.user.lastName,
        imagePath: '',
        isLoading: false
    }

    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()

                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                this.setState({ isLoading: true })
                axios.post('https://zenon.onthewifi.com/moneyGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            isLoading: false,
                        })
                    })
                    .catch(error => {
                        this.setState({ isLoading: false })
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }


    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        this.setState({ isLoading: true })
        axios.get('https://zenon.onthewifi.com/moneyGo/users', {
            headers: {
                Authorization: `Bearer ${this.props.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => {
                this.setState({ isLoading: false })
                console.log(err)
            })
            .finally(() => { console.log('Finally') })
    }


    onSave = () => {
        axios({
            url: 'https://zenon.onthewifi.com/moneyGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/moneyGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.token}`
                },
            }
            )
        })
            .then(response => {
                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/profile', {
                    item: 'profile'
                })
            }).catch(e => {
                console.log("error " + e)
            })

    }


    goToProfile = () => {
        this.props.history.push('./profile')
    }

    goToMain = () => {
        this.props.history.push('./main')
    }

    goToWallet = () => {
        this.props.history.push('./wallet')
    }

    render() {
        const { user, editUser } = this.props
        return (
            <ImageBackground source={bg} style={styles.container}>
                <Text style={styles.text}>Edit Profile</Text>
                <TouchableOpacity onPress={this.selectImage}>
                    <ImageBackground source={logo} style={styles.image}>
                        <Image source={{ uri: this.state.imagePath }} style={styles.image} />
                    </ImageBackground>
                </TouchableOpacity>

                <Text></Text>

                <View style={styles.inputtex}>
                    <InputItem
                        value={this.state.firstName}
                        onChange={value => { this.setState({ firstName: value }) }}
                        placeholder='First Name'
                        style={styles.inputtext}
                    />
                    {/* <Icon style={styles.icon} name={"user"} /> */}
                </View>


                <Text></Text>

                <View style={styles.inputtex}>
                    <InputItem
                        value={this.state.lastName}
                        onChange={value => { this.setState({ lastName: value }) }}
                        placeholder='Last Name'
                        style={styles.inputtext}
                    />
                    {/* <Icon style={styles.icon} name={"user"} /> */}
                </View>

                <Text></Text>

                <View style={styles.row}>
                    <TouchableOpacity>
                        <Button type="primary"
                           onPress={() => this.onSave()}>SAVE</Button>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Button type="warning" style={{ marginLeft: 20 }}
                            onPress={this.goToProfile}>CANCEL</Button>
                    </TouchableOpacity>
                </View>

            </ImageBackground>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: 'white',
        fontSize: 50,
        fontWeight: 'bold',
        padding: 20
    },
    text1: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
        padding: 20
    },
    image: {
        width: 150,
        height: 150,
    },
    inputtex: {
        borderWidth: 1,
        borderColor: '#d7dae0',
        borderRadius: 100,
        paddingHorizontal: 14,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',

    },
    row: {
        flexDirection: 'row',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 37
    },
    listWallet: {
        flex: 1,
        top: 30,
    },
    footer: {
        borderWidth: 3,
        borderColor: 'white',
        flexDirection: 'row'
    },
    iconbar: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'EDIT_USER',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)

