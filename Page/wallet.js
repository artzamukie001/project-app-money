import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert, TextInput, ImageBackground, ScrollView, FlatList } from 'react-native';
import bg from '../image/357.jpg'
import bg2 from '../image/3.jpg'
import logo from '../image/55.jpg'
import { Button, Icon, Card, WingBlank } from '@ant-design/react-native';
import { connect } from 'react-redux'

class wallet extends React.Component {


    goToMain = () => {
        this.props.history.push('./main')
    }

    goToWallet = () => {
        this.props.history.push('./wallet')
    }

    goToProfile = () => {
        this.props.history.push('./profile')
    }

    goToAddWallet = () => {
        this.props.history.push('./AddWallet')
    }

    goToMyWallet = () => {
        this.props.history.push('./MyWallet')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <ImageBackground style={styles.iconhead1} source={bg}>
                        <TouchableOpacity>
                            <Icon size='lg' color='white' name={'arrow-left'}
                                onPress={this.goToMain} />
                        </TouchableOpacity>
                    </ImageBackground>

                    <ImageBackground style={styles.iconhead2} source={bg}>

                        <Text style={styles.text}>Wallet</Text>
                    </ImageBackground>

                </View>
                <View style={styles.body}>
                    <ImageBackground source={bg2} style={{ width: '100%', height: '100%' }}>

                        <ScrollView style={styles.listWallet}>
                            <WingBlank size="lg">


                            </WingBlank>
                        </ScrollView>

                    </ImageBackground>
                </View>

                <View style={styles.footer}>
                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity >
                            <Icon size='lg' color='white' name={'home'} onPress={this.goToMain} />
                        </TouchableOpacity>
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToWallet} >
                            <Icon size='lg' color='white' name={'wallet'} />
                        </TouchableOpacity>
                    </ImageBackground>


                    <ImageBackground style={styles.iconbar} source={bg}>
                        <Icon size='lg' color='white' name={'bell'} />
                    </ImageBackground>

                    <ImageBackground style={styles.iconbar} source={bg}>
                        <TouchableOpacity onPress={this.goToProfile} >
                            <Icon size='lg' color='white' name={'user'} />
                        </TouchableOpacity>
                    </ImageBackground>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    header: {
        borderWidth: 5,
        borderColor: 'white',
        flexDirection: 'row'
    },
    iconhead1: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    iconhead2: {
        flex: 5,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    body: {
        flex: 1,
        flexDirection: 'column'
    },
    footer: {
        borderWidth: 3,
        borderColor: 'white',
        flexDirection: 'row'
    },
    text: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center'

    },
    text1: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 20,
        textAlign: 'center',
        top: -15
    },
    amountMoney: {

    },
    listWallet: {
        flex: 1,
        top: 30
    },
    iconbar: {
        flex: 1,
        alignItems: 'center',
        borderWidth: 2,
        borderColor: 'white',
    },
    image: {
        width: 120,
        height: 120,
        borderRadius: 100,
        top: -55,
        left: 5
    },
    inputtex: {
        backgroundColor: 'white',
        flexDirection: 'column',
        alignItems: 'center',
        width: 260,
        height: 90,
        margin: 14,
        top: 65,
        left: 60,
        borderRadius: 100,
    },
    wallet: {
        height: 80,
        backgroundColor: '#f4f4f4',
        paddingHorizontal: 24,
        margin: 8,
        borderRadius: 6,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    icon: {
        position: 'absolute',
        top: 12,
        left: 37,
    },

})

export default wallet

