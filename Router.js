import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { ConnectedRouter } from 'connected-react-router'
import { Provider } from 'react-redux'
import { store, history } from './AppStore'

import Login from './Page/Login'
import Register from './Page/Register'
import main from './Page/main'
import wallet from './Page/wallet'
import revenue from './Page/revenue'
import expense from './Page/expense'
import AddWallet from './Page/AddWallet'
import profile from './Page/profile'
import MyWallet from './Page/MyWallet'
import EditProfile from './Page/EditProfile'


class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/Login" component={Login} />
                        <Route exact path="/Register" component={Register} />
                        <Route exact path="/main" component={main} />
                        <Route exact path="/wallet" component={wallet} />
                        <Route exact path="/revenue" component={revenue} />
                        <Route exact path="/expense" component={expense} />
                        <Route exact path="/AddWallet" component={AddWallet} />
                        <Route exact path="/profile" component={profile} />
                        <Route exact path="/MyWallet" component={MyWallet} />
                        <Route exact path="/EditProfile" component={EditProfile} />
                        <Redirect to="/Login" />
                    </Switch>
                </ConnectedRouter>
            </Provider>


        )
    }
}

export default Router